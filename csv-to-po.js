#!/usr/bin/env node
'use strict';

var args = require('minimist')(process.argv.slice(2));

if (args._.length !== 1) {
    console.log('csv-to-po requires 1 input argument! Run it like this: "csv-to-po.js input.csv"');
    process.exit(1);
}

if ((args._[0] + '').indexOf('.csv') === -1) {
    console.log('csv-to-po requires the 1st argument to be a path to the csv source!');
    process.exit(1);
}

if ((args.project || '').length < 2) {
    console.log('"--project" flag must be specified!');
    process.exit(1);
}

if ((args.version || '').length < 2) {
    console.log('"--version" flag must be specified!');
    process.exit(1);
}


var fs = require('fs'),
    Converter = require('csvtojson').Converter,
    converter = new Converter({});

var date = new Date().toISOString().slice(0, 19).replace('T', ' ');

converter.fromString(fs.readFileSync(args._[0], 'utf8'), function (err, parsed) {
    var pos = {};

    parsed.splice(0, 1); // removing headers

    parsed.forEach(function (line) {
        for (var cell in line) {
            if (line.hasOwnProperty(cell)) {
                if (cell.length === 2) {
                    if (!pos[cell]) {
                        pos[cell] = {}
                    }
                    pos[cell][line['Message context']] = line[cell];
                }
            }
        }
    });
    for (var language in pos) {
        if (pos.hasOwnProperty(language)) {
            writePoFile(pos[language], language);
        }
    }
});


function writePoFile(data, lang) {

    var file = 'msgid ""\n' +
        'msgstr ""\n' +
        '"Plural-Forms: nplurals=2; plural=(n != 1);\\n"\n' +
        '"Project-Id-Version: ' + args.project + '\\n"\n' +
        '"POT-Creation-Date: \\n"\n' +
        '"PO-Revision-Date: ' + date + '\\n"\n' +
        '"Last-Translator: \\n"\n' +
        '"Language-Team: Aporta Digital <ds@aportadigital.com>\\n"\n' +
        '"MIME-Version: 1.0\\n"\n' +
        '"Content-Type: text/plain; charset=UTF-8\\n"\n' +
        '"Content-Transfer-Encoding: 8bit\\n"\n' +
        '"Language: ' + lang + '\\n"\n' +
        '"X-Generator: csv-to-po-generator\\n"\n' +
        '"X-Poedit-SourceCharset: UTF-8\\n"';

    for (var token in data) {
        if (data.hasOwnProperty(token)) {
            file += '\n\nmsgctxt "' + token + '"\nmsgid "' + data[token] + '"\nmsgstr "' + data[token] + '"';
        }
    }

    fs.writeFileSync(args.project + '.' + lang + '-' + args.version + '.po', file, 'utf8');
    console.log(lang + ' language exported!');
}
